BibleBooks
==========

06/20/2015 - version 0.1.1

This is a class containing an array of the books of the Bible with the number of chapters and verses in each along 
with some useful methods.

Installation:

Simply add 
"snap/bible-books": "0.1.*"
to your composer.json file
or download and manually require the file.
Note: You will also need to add "minimum-stability": "dev" to your composer.json file.

Simple Usage:

```php
// require the class file if you aren't using an autoloader
require_once('path/to/BibleBooks.php');
// create the object
$BibleBooks = new \Snap\BibleBooks\BibleBooks();
// get an array of the books
$books = $BibleBooks->getArrayOfBooks(); // a big array
// get the number of chapters in a book
$number_of_chapters = $BibleBooks->getNumberOfChapters('John'); // 21
// get an array of the chapters for a book
$chapters = $BibleBooks->getArrayOfChapters('John'); // a small array
// get the number of verses in a book's chapter
$number_of_verses = $BibleBooks->getNumberOfVerses('John', 3); // 36
```

I hope that this class is helpful for you.  It it is, consider showing your appreciation by:

1. Letting me know about any errors you might find via a pull request
2. Sending me a kind word telling me how it helped you
3. Hiring me for your next PHP project


Thanks, enjoy, happy PHPing and God bless, 

Alex Fraundorf

AlexFraundorf.com
